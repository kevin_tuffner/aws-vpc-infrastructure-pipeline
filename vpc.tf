# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Create AWS VPC With Terraform
resource "aws_vpc" "Tuffner-Demo" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "Tuffner-Demo"
  }
}

# Create AWS Public Subnet #1 in VPC Just Created 
resource "aws_subnet" "pubsubnet1" {
  vpc_id     = aws_vpc.Tuffner-Demo.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public Subnet 1"
  }
}

# Create AWS Internet Gateway for VPC Just Created
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.Tuffner-Demo.id

  tags = {
    Name = "Tuffner-Demo-VPC-IGW"
  }
}
